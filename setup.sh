#!/usr/bin/env bash

echo "Starting installation of CashCountPi"
cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"

sed -i -- "s/https:\/\/gitlab.com\/rpiguru\/CashCountPi/git@gitlab.com:rpiguru\/CashCountPi.git/g" .git/config

sudo apt install -y python3-dev python3-pip

sudo pip3 install -U pip3
sudo pip3 install -r requirements.txt

# Enable auto start
sudo apt install screen

sudo sed -i -- "s/^exit 0/screen -mS ccp -d\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S ccp -X stuff \"cd \/home\/pi\/CashCountPi\\\\r\"\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S ccp -X stuff \"python3 main.py\\\\r\"\\nexit 0/g" /etc/rc.local
