import os
import signal
import subprocess
import logging.config


def is_rpi():
    try:
        return 'arm' in os.uname()[4]
    except AttributeError:
        return False


_cur_dir = os.path.dirname(os.path.realpath(__file__))
logging.config.fileConfig(os.path.join(_cur_dir, 'logging.ini'))
logger = logging.getLogger('CCP')


def get_serial():
    """
    Get serial number of the device
    :return:
    """
    if is_rpi():
        cpuserial = "0000000000000000"
        f = open('/proc/cpuinfo', 'r')
        for line in f:
            if line[0:6] == 'Serial':
                cpuserial = line[10:26].lstrip('0')
        f.close()
        return cpuserial
    else:
        return '12345678'


def check_running_proc(proc_name):
    """
    Check if a process is running or not
    :param proc_name:
    :return:
    """
    try:
        if len(os.popen("ps -aef | grep -i '%s' "
                        "| grep -v 'grep' | awk '{ print $3 }'" % proc_name).read().strip().splitlines()) > 0:
            return True
    except Exception as e:
        logger.error('Failed to get status of the process({}) - {}'.format(proc_name, e))
    return False


def kill_process_by_name(proc_name):
    """
    Kill process by its name
    :param proc_name:
    :return:
    """
    p = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    for line in out.decode().splitlines():
        if proc_name in line:
            pid = int(line.split(None, 1)[0])
            print('Found PID({}) of `{}`, killing...'.format(pid, proc_name))
            os.kill(pid, signal.SIGKILL)


def check_git_commit():
    """
    Compare local/remote commit datetime, and pull if any updates in the remote repo.
    :return:
    """
    p = subprocess.Popen('/bin/bash {}/git_update.sh'.format(_cur_dir), shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    p.wait()

    return stdout.decode(), stderr.decode()
