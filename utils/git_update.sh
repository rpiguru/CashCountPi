#!/usr/bin/env bash

cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"

echo "Starting ssh-agent"
eval "$(ssh-agent -s)"

chmod 400 ${cur_dir}/../cert/deploy_cash_count_pi*
echo "Adding ssh-key"
ssh-add ${cur_dir}/../cert/deploy_cash_count_pi

cd ${cur_dir}/..
git reset --hard

echo "Pulling remote repo"
git pull
