import requests

from settings import SERVER_HOST
from utils.common import get_serial


def upload_data(payload):
    url = "http://{}/api/v1/cash_count".format(SERVER_HOST)
    headers = {'sn': get_serial(), 'Content-Type': 'application/json'}
    response = requests.post(url=url, headers=headers, json=payload, timeout=10)
    return response.status_code


def get_last_data():
    url = "http://{}/api/v1/cash_count".format(SERVER_HOST)
    headers = {'sn': get_serial(), 'Content-Type': 'application/json'}
    response = requests.get(url=url, headers=headers)
    if response.status_code == 200:
        return response.json()


if __name__ == '__main__':
    import datetime
    print(upload_data({'data': 'Here is a testing data - {}'.format(datetime.datetime.now())}))
    print(get_last_data())
