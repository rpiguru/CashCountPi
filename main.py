import os
import subprocess
import threading
import time
import serial.tools.list_ports

from settings import BAUDRATE, TIMEOUT, USB_INFO
from utils.api import upload_data
from utils.common import logger, check_git_commit, is_rpi


class CashCountPi(threading.Thread):

    def __init__(self):
        super().__init__()
        self._ser = None
        self._buf = []

    def get_serial_port(self):
        port = None
        for dev in serial.tools.list_ports.comports():
            for info in USB_INFO:
                if dev.vid == info[0] and dev.pid == info[1]:
                    port = dev.device
                    logger.info('Found the serial converter - {}, info: {}'.format(port, info))
        if port is not None:
            try:
                self._ser = serial.Serial(port=port, baudrate=BAUDRATE, timeout=TIMEOUT)
                return True
            except Exception as e:
                logger.error('Failed to open port - {} - {}'.format(port, e))

    def run(self):
        while True:
            try:
                if self._ser is None:
                    if not self.get_serial_port():
                        time.sleep(1)
                        continue
                line = self._ser.readline()
                if line:
                    logger.debug('Read a line - `{}`'.format(line))
                    if line == b'\r\x03':
                        data = [b.decode().strip() for b in self._buf]
                        logger.info('=== Uploading data to server - `{}`'.format(data))
                        upload_data(payload={'data': '\n'.join(data)})
                        self._buf = []
                    else:
                        self._buf.append(line)
            except Exception as e:
                print('Failed to read data. Disconnected? - {}'.format(e))
                self._ser = None
            time.sleep(.3)


def check_remote_repo():

    while True:
        time.sleep(120)
        msg, err = check_git_commit()
        if 'Fast-forward' in msg:
            logger.warning('===== Remote repository was updated! Restart me after pulling...')
            logger.warning(msg)
            break
        time.sleep(10)
        # Kill ssh-agent
        proc = subprocess.Popen(["pkill", "-f", "ssh-agent"], stdout=subprocess.PIPE)
        proc.wait()
    if is_rpi():
        os.system('reboot')


if __name__ == '__main__':

    logger.info('========== Starting CashCountPi ==========')

    threading.Thread(target=check_remote_repo).start()

    ccp = CashCountPi()
    ccp.start()
