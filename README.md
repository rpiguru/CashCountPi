# CashCountPi

Cash Count RPi

## Components

- Raspberry Pi Model B+

    https://www.amazon.com/gp/product/B01D92SSX6/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1

- Serial USB Reader
    
    https://www.amazon.com/gp/product/B0000VYJRY/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1

## Installation

- Download the latest Raspbian Stretch from [here](https://www.raspberrypi.org/downloads/) and flash your Micro SD card.

- Install our service

        
    sudo apt update
    sudo apt install git

    cd ~
    git clone https://gitlab.com/rpiguru/CashCountPi
    cd CashCountPi
    bash setup.sh

- And reboot!
 